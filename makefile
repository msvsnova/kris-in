#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - IN - MAKEFILE
# By FIST -  Kresimir Mandic 27.03.1999
#
#------------------------------------------------------------------------------

# Compiler options
#------------------------------------------------------------------------------
CC          = start /w c:\vscd\bin\nova


CFLAGS      = -w -ereadfgl

TD			= X:\in\ 
#TD			= T:\in\ 

YTD			= Y:\NewVer.RAR\in\ 
AD          = Y:\in\ 
COMD        = .\lib\incommon.fle 

INFILES  =  $(TD)asci_in.fle  $(TD)indbutil.fle $(TD)in.fle  $(TD)inartikl.fle  \
     $(TD)ingrin.fle   $(TD)ininpg.fle   $(TD)inknjige.fle $(TD)innar.fle     \
     $(TD)lkalkura.fle $(TD)uzrac_st.fle $(TD)inpg_tms.fle $(TD)in_start.fle  \
     $(TD)inpogrin.fle $(TD)inpon.fle    $(TD)inponipg.fle $(TD)inracnar.fle  \
     $(TD)ins_inpg.fle $(TD)ins_nar.fle  $(TD)ins_pon.fle  $(TD)ins_upit.fle  \
     $(TD)inupit.fle   $(TD)invrpost.fle $(TD)inzah.fle    $(TD)inzelje.fle   \
     $(TD)in_dd.fle    $(TD)in_flds.fle  $(TD)in_reg.fle   $(TD)l_artikl.fle  \
     $(TD)l_grin.fle   $(TD)l_inpg.fle   $(TD)l_nar.fle    $(TD)l_pogrin.fle  \
     $(TD)l_pon.fle    $(TD)l_real.fle   $(TD)l_sinpg.fle  $(TD)l_spon.fle    \
     $(TD)iis_zah.fle  $(TD)l_urzap.fle  $(TD)l_uzrac.fle  $(TD)l_zah.fle     \
     $(TD)l_zelje.fle  $(TD)mkarttms.fle $(TD)uzracuni.fle $(TD)inar_tms.fle  \
     $(TD)mali_pos.fle $(TD)l_knjige.fle $(TD)l_vrpost.fle $(TD)l_snar.fle    \
     $(TD)ins_zelj.fle $(TD)innartms.fle $(TD)ins_zah.fle  $(TD)zeljetms.fle  \
     $(TD)zahtms.fle   $(TD)inugo.fle    $(TD)ins_ugo.fle  $(TD)iis_nar.fle   \
     $(TD)innaripg.fle $(TD)in_ngod.fle  $(TD)uzr_fast.fle $(TD)in_spec.fle   \
     $(TD)lin_spec.fle $(TD)in_ascii.fle $(TD)in_idx.fle   $(TD)senders.fle   \
     $(TD)inpren_rd.fle $(TD)inugotms.fle $(TD)in_rev.fle  $(TD)gt_rez.fle   \
     $(TD)vrs_vanbil.fle $(TD)narudzbenice.fle $(TD)st_narudzbe.fle $(TD)l_narudzba.fle \
     $(TD)jnpostupci.fle $(TD)jnabava.fle $(TD)jntroskovnik.fle $(TD)jnugovori.fle \
     $(TD)l_jnabava.fle  $(TD)load_jn.fle $(TD)hpajn_stavke.fle 

COMMON    = in_dd.ls in_ddx.ls in_flds.ls inknjige.ls inartikl.ls ufa_pdvs.ls view_pdvs.ls
            

all      : $(INFILES) 

## COMMON

in_dd.ls:  in_dd.lgc
    $(CC) $(CFLAGS) appl=in_dd dotfle=.\lib\in_dd.fle 
    @echo " ">in_dd.ls

in_ddx.ls:  in_ddx.lgc
    $(CC) $(CFLAGS) appl=in_ddx dotfle=.\lib\in_dd.fle 
    @echo " ">in_ddx.ls

in_flds.ls:  in_flds.lgc
    $(CC) $(CFLAGS) appl=in_flds dotfle=.\lib\in_dd.fle 
    @echo " ">in_flds.ls

inknjige.ls:  inknjige.lgc
    $(CC) $(CFLAGS) appl=inknjige dotfle=$(COMD)
    @echo " ">inknjige.ls

inartikl.ls:  inartikl.lgc
    $(CC) $(CFLAGS) appl=inartikl dotfle=$(COMD)
    @echo " ">inartikl.ls

ufa_pdvs.ls:  ufa_pdvs.lgc
    $(CC) $(CFLAGS) appl=ufa_pdvs dotfle=$(COMD)
    @echo " ">ufa_pdvs.ls

view_pdvs.ls:  view_pdvs.lgc
    $(CC) $(CFLAGS) appl=view_pdvs dotfle=$(COMD)
    @echo " ">view_pdvs.ls

###################3
$(TD)in.fle:  in.lgc
    $(CC) $(CFLAGS) appl=in dotfle=$(TD)in.fle

$(TD)in_dd.fle:  in_dd.lgc
    $(CC) $(CFLAGS) appl=in_dd dotfle=$(TD)in_dd.fle
    $(CC) -w genddx ddfile=in_dd
    $(CC) $(CFLAGS) appl=in_ddx dotfle=$(TD)in_dd.fle

$(TD)asci_in.fle:  asci_in.lgc
    $(CC) $(CFLAGS) appl=asci_in dotfle=$(TD)asci_in.fle

$(TD)indbutil.fle:  indbutil.lgc
    $(CC) $(CFLAGS) appl=indbutil dotfle=$(TD)indbutil.fle

$(TD)inartikl.fle:  inartikl.lgc
    $(CC) $(CFLAGS) appl=inartikl dotfle=$(TD)inartikl.fle

$(TD)ingrin.fle:  ingrin.lgc
    $(CC) $(CFLAGS) appl=ingrin dotfle=$(TD)ingrin.fle

$(TD)ininpg.fle:  ininpg.lgc
    $(CC) $(CFLAGS) appl=ininpg dotfle=$(TD)ininpg.fle

$(TD)inknjige.fle:  inknjige.lgc
    $(CC) $(CFLAGS) appl=inknjige dotfle=$(TD)inknjige.fle

$(TD)innar.fle:  innar.lgc
    $(CC) $(CFLAGS) appl=innar dotfle=$(TD)innar.fle

$(TD)innaripg.fle:  innaripg.lgc
    $(CC) $(CFLAGS) appl=innaripg dotfle=$(TD)innaripg.fle

$(TD)inpg_tms.fle:  inpg_tms.lgc
    $(CC) $(CFLAGS) appl=inpg_tms dotfle=$(TD)inpg_tms.fle

$(TD)inpogrin.fle:  inpogrin.lgc
    $(CC) $(CFLAGS) appl=inpogrin dotfle=$(TD)inpogrin.fle

$(TD)inpon.fle:  inpon.lgc
    $(CC) $(CFLAGS) appl=inpon dotfle=$(TD)inpon.fle

$(TD)inponipg.fle:  inponipg.lgc
    $(CC) $(CFLAGS) appl=inponipg dotfle=$(TD)inponipg.fle

$(TD)inracnar.fle:  inracnar.lgc
    $(CC) $(CFLAGS) appl=inracnar dotfle=$(TD)inracnar.fle

$(TD)ins_inpg.fle:  ins_inpg.lgc
    $(CC) $(CFLAGS) appl=ins_inpg dotfle=$(TD)ins_inpg.fle

$(TD)ins_nar.fle:  ins_nar.lgc
    $(CC) $(CFLAGS) appl=ins_nar dotfle=$(TD)ins_nar.fle

$(TD)ins_pon.fle:  ins_pon.lgc
    $(CC) $(CFLAGS) appl=ins_pon dotfle=$(TD)ins_pon.fle

$(TD)ins_upit.fle:  ins_upit.lgc
    $(CC) $(CFLAGS) appl=ins_upit dotfle=$(TD)ins_upit.fle

$(TD)inupit.fle:  inupit.lgc
    $(CC) $(CFLAGS) appl=inupit dotfle=$(TD)inupit.fle

$(TD)invrpost.fle:  invrpost.lgc
    $(CC) $(CFLAGS) appl=invrpost dotfle=$(TD)invrpost.fle

$(TD)inzah.fle:  inzah.lgc
    $(CC) $(CFLAGS) appl=inzah dotfle=$(TD)inzah.fle

$(TD)inzelje.fle:  inzelje.lgc
    $(CC) $(CFLAGS) appl=inzelje dotfle=$(TD)inzelje.fle

$(TD)in_flds.fle:  in_flds.lgc
    $(CC) $(CFLAGS) appl=in_flds dotfle=$(TD)in_flds.fle

$(TD)in_reg.fle:  in_reg.lgc
    $(CC) $(CFLAGS) appl=in_reg dotfle=$(TD)in_reg.fle

$(TD)l_artikl.fle:  l_artikl.lgc
    $(CC) $(CFLAGS) appl=l_artikl dotfle=$(TD)l_artikl.fle

$(TD)l_grin.fle:  l_grin.lgc
    $(CC) $(CFLAGS) appl=l_grin dotfle=$(TD)l_grin.fle

$(TD)l_inpg.fle:  l_inpg.lgc
    $(CC) $(CFLAGS) appl=l_inpg dotfle=$(TD)l_inpg.fle

$(TD)l_nar.fle:  l_nar.lgc
    $(CC) $(CFLAGS) appl=l_nar dotfle=$(TD)l_nar.fle

$(TD)l_pogrin.fle:  l_pogrin.lgc
    $(CC) $(CFLAGS) appl=l_pogrin dotfle=$(TD)l_pogrin.fle

$(TD)l_pon.fle:  l_pon.lgc
    $(CC) $(CFLAGS) appl=l_pon dotfle=$(TD)l_pon.fle

$(TD)l_real.fle:  l_real.lgc
    $(CC) $(CFLAGS) appl=l_real dotfle=$(TD)l_real.fle

$(TD)l_sinpg.fle:  l_sinpg.lgc
    $(CC) $(CFLAGS) appl=l_sinpg dotfle=$(TD)l_sinpg.fle

$(TD)l_snar.fle:  l_snar.lgc
    $(CC) $(CFLAGS) appl=l_snar dotfle=$(TD)l_snar.fle

$(TD)l_spon.fle:  l_spon.lgc
    $(CC) $(CFLAGS) appl=l_spon dotfle=$(TD)l_spon.fle

$(TD)l_urzap.fle:  l_urzap.lgc
    $(CC) $(CFLAGS) appl=l_urzap dotfle=$(TD)l_urzap.fle

$(TD)l_uzrac.fle:  l_uzrac.lgc
    $(CC) $(CFLAGS) appl=l_uzrac dotfle=$(TD)l_uzrac.fle

$(TD)l_zah.fle:  l_zah.lgc
    $(CC) $(CFLAGS) appl=l_zah dotfle=$(TD)l_zah.fle

$(TD)l_zelje.fle:  l_zelje.lgc
    $(CC) $(CFLAGS) appl=l_zelje dotfle=$(TD)l_zelje.fle

$(TD)mkarttms.fle:  mkarttms.lgc
    $(CC) $(CFLAGS) appl=mkarttms dotfle=$(TD)mkarttms.fle

$(TD)uzracuni.fle:  uzracuni.lgc
    $(CC) $(CFLAGS) appl=uzracuni dotfle=$(TD)uzracuni.fle

$(TD)uzrac_st.fle:  uzrac_st.lgc
    $(CC) $(CFLAGS) appl=uzrac_st dotfle=$(TD)uzrac_st.fle

$(TD)inar_tms.fle:  inar_tms.lgc
    $(CC) $(CFLAGS) appl=inar_tms dotfle=$(TD)inar_tms.fle
    
$(TD)mali_pos.fle:  mali_pos.lgc
    $(CC) $(CFLAGS) appl=mali_pos dotfle=$(TD)mali_pos.fle
    
$(TD)l_knjige.fle:  l_knjige.lgc
    $(CC) $(CFLAGS) appl=l_knjige dotfle=$(TD)l_knjige.fle
    
$(TD)l_vrpost.fle:  l_vrpost.lgc
    $(CC) $(CFLAGS) appl=l_vrpost dotfle=$(TD)l_vrpost.fle
    
$(TD)ins_zelj.fle:  ins_zelj.lgc
    $(CC) $(CFLAGS) appl=ins_zelj dotfle=$(TD)ins_zelj.fle
    
$(TD)innartms.fle:  innartms.lgc
    $(CC) $(CFLAGS) appl=innartms dotfle=$(TD)innartms.fle
    
$(TD)ins_zah.fle:  ins_zah.lgc
    $(CC) $(CFLAGS) appl=ins_zah dotfle=$(TD)ins_zah.fle
    
$(TD)zeljetms.fle:  zeljetms.lgc
    $(CC) $(CFLAGS) appl=zeljetms dotfle=$(TD)zeljetms.fle
    
$(TD)zahtms.fle:  zahtms.lgc
    $(CC) $(CFLAGS) appl=zahtms dotfle=$(TD)zahtms.fle
    
$(TD)inugo.fle:  inugo.lgc
    $(CC) $(CFLAGS) appl=inugo dotfle=$(TD)inugo.fle
    
$(TD)inugotms.fle:  inugotms.lgc
    $(CC) $(CFLAGS) appl=inugotms dotfle=$(TD)inugotms.fle
    
$(TD)ins_ugo.fle:  ins_ugo.lgc
    $(CC) $(CFLAGS) appl=ins_ugo dotfle=$(TD)ins_ugo.fle
    
$(TD)iis_zah.fle:  iis_zah.lgc
    $(CC) $(CFLAGS) appl=iis_zah dotfle=$(TD)iis_zah.fle
    
$(TD)lkalkura.fle:  lkalkura.lgc
    $(CC) $(CFLAGS) appl=lkalkura dotfle=$(TD)lkalkura.fle
    
$(TD)iis_nar.fle:  iis_nar.lgc
    $(CC) $(CFLAGS) appl=iis_nar dotfle=$(TD)iis_nar.fle

$(TD)in_ngod.fle:  in_ngod.lgc
    $(CC) $(CFLAGS) appl=in_ngod dotfle=$(TD)in_ngod.fle

$(TD)uzr_fast.fle:  uzr_fast.lgc
    $(CC) $(CFLAGS) appl=uzr_fast dotfle=$(TD)uzr_fast.fle

$(TD)in_spec.fle:  in_spec.lgc
    $(CC) $(CFLAGS) appl=in_spec dotfle=$(TD)in_spec.fle

$(TD)lin_spec.fle:  lin_spec.lgc
    $(CC) $(CFLAGS) appl=lin_spec dotfle=$(TD)lin_spec.fle

$(TD)in_ascii.fle:  in_ascii.lgc
    $(CC) $(CFLAGS) appl=in_ascii dotfle=$(TD)in_ascii.fle
    $(CC) $(CFLAGS) appl=in_ascii dotfle=$(AD)in_ascii.fle
    @echo " ">  in_ascii.ts      

$(TD)in_idx.fle:  in_idx.lgc
    $(CC) $(CFLAGS) appl=in_idx dotfle=$(TD)in_idx.fle

$(TD)senders.fle:  senders.lgc
    $(CC) $(CFLAGS) appl=senders dotfle=$(TD)senders.fle

$(TD)in_start.fle:  in_start.lgc
    $(CC) $(CFLAGS) appl=in_start dotfle=$(TD)in_start.fle

$(TD)inpren_rd.fle:  inpren_rd.lgc
    $(CC) $(CFLAGS) appl=inpren_rd dotfle=$(TD)inpren_rd.fle

$(TD)in_rev.fle:  in_rev.lgc
    $(CC) $(CFLAGS) appl=in_rev dotfle=$(TD)in_rev.fle

$(TD)gt_rez.fle:  gt_rez.lgc
    $(CC) $(CFLAGS) appl=gt_rez dotfle=$(TD)gt_rez.fle

$(TD)vrs_vanbil.fle:  vrs_vanbil.lgc
    $(CC) $(CFLAGS) appl=vrs_vanbil dotfle=$(TD)vrs_vanbil.fle

$(TD)jnpostupci.fle:  jnpostupci.lgc
    $(CC) $(CFLAGS) appl=jnpostupci dotfle=$(TD)jnpostupci.fle

$(TD)jnabava.fle:  jnabava.lgc
    $(CC) $(CFLAGS) appl=jnabava dotfle=$(TD)jnabava.fle

$(TD)jntroskovnik.fle:  jntroskovnik.lgc
    $(CC) $(CFLAGS) appl=jntroskovnik dotfle=$(TD)jntroskovnik.fle

$(TD)jnugovori.fle:  jnugovori.lgc
    $(CC) $(CFLAGS) appl=jnugovori dotfle=$(TD)jnugovori.fle

$(TD)l_jnabava.fle:  l_jnabava.lgc
    $(CC) $(CFLAGS) appl=l_jnabava dotfle=$(TD)l_jnabava.fle

$(TD)load_jn.fle:  load_jn.lgc
    $(CC) $(CFLAGS) appl=load_jn dotfle=$(TD)load_jn.fle

$(TD)hpajn_stavke.fle:  hpajn_stavke.lgc
    $(CC) $(CFLAGS) appl=hpajn_stavke dotfle=$(TD)hpajn_stavke.fle

$(TD)narudzbenice.fle:  narudzbenice.lgc
    $(CC) $(CFLAGS) appl=narudzbenice dotfle=$(TD)narudzbenice.fle

$(TD)st_narudzbe.fle:  st_narudzbe.lgc
    $(CC) $(CFLAGS) appl=st_narudzbe dotfle=$(TD)st_narudzbe.fle

$(TD)l_narudzba.fle:  l_narudzba.lgc
    $(CC) $(CFLAGS) appl=l_narudzba dotfle=$(TD)l_narudzba.fle

